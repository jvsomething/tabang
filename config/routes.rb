Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: 'registrations'
  }

  namespace :grocery do
    get 'weekly'
    get 'daily'
    get 'custom'
  end

  get 'location', to: 'pages#location'
  get 'task_details', to: 'pages#task_details'
  get 'cart', to: 'pages#cart'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'pages#home'
end
