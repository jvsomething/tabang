class GroceryController < ApplicationController
  protect_from_forgery with: :exception

  def weekly
    @day_start = Date.today.monday.next_week
  end

  def daily
  end

  def custom
  end
end
