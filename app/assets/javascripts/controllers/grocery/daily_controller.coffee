app.controller('DailyCtrl', ['$scope',
  ($scope) ->
    $scope.init = ->
      $('.td-meal').each( ->
        $this = $(this)
        $this.popover({
          trigger: 'hover',
          html: true,
          container: $this,
          delay: {
            show: 500
          }
          content: ->
            $('#mealName').html($this.find('.btn-meal').text())
            $popoverElem = $('#popoverTemplate')
            $popoverElem.html()
        })
      )
      $('.btn-meal-md').click( ->
        $('[data-toggle=popover]').popover('hide')
        $('#searchMealModal').modal('show')
      )
      $('.nav-tabs').tab()
      true

    $scope.init()
])
