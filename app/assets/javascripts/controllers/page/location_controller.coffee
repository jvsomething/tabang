app.controller('LocationCtrl', ['$scope', 'NgMap',
  ($scope, NgMap) ->
    $scope.init = ->
      $scope.showFormOne()
      true

    $scope.showFormTwo = ->
      $('#step-one').hide()
      $('#step-two').show()
      NgMap.getMap().then((map)->
        center = map.getCenter()
        google.maps.event.trigger(map, "resize")
        map.setCenter(center)
      )
      false

    $scope.showFormOne = ->
      $('#step-one').show()
      $('#step-two').hide()
      false

    $scope.init()
])
