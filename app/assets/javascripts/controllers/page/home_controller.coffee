app.controller('HomeCtrl', ['$scope',
  ($scope) ->
    states = [
      { name: 'Laundry', img_class: 'cleaning_img' },
      { name: 'Grocery Shopping', img_class: 'shopping_img', url: '/grocery/custom'},
      { name: 'Unit Cleaning', img_class: 'cleaning_img' },
      { name: 'Personal Assistance', img_class: 'legal_aide_img' },
      { name: 'Salon Services', img_class: 'salon_img' },

      { name: 'Manicure', img_class: 'salon_img' },

      { name: 'Cleaning', img_class: 'cleaning_img' },
      { name: 'Salon Services', img_class: 'salon_img' },
      { name: 'Pedicure', img_class: 'salon_img' },
      { name: 'Hair Dye', img_class: 'salon_img' },
      { name: 'Haircut', img_class: 'salon_img' },
      { name: 'Massage', img_class: 'salon_img' },
      { name: 'Personal Assistance', img_class: 'legal_aide_img' },
      { name: 'Research Assistance', img_class: 'legal_aide_img' },
      { name: 'Clerical Assistance', img_class: 'legal_aide_img' },

      #{ name: 'General House Cleaning', img_class: 'cleaning_img' },
      #{ name: 'Refrigerator repair', img_class: 'repair_img' },
      #{ name: 'Grocery Shopping', img_class: 'shopping_img' },
      #{ name: 'Manicure', img_class: 'salon_img' },
      #{ name: 'Babysitting', img_class: 'babysitting_img' },
      #{ name: 'Business Permits', img_class: 'legal_aide_img' },


      #{ name: 'Bathroom Cleaning', img_class: 'cleaning_img' },
      #{ name: 'Car Wash', img_class: 'cleaning_img' },
      #{ name: 'Laundry', img_class: 'cleaning_img' },

      #{ name: 'Air conditioning repair', img_class: 'repair_img' },
      #{ name: 'Computer repair', img_class: 'repair_img' },
      #{ name: 'TV repair', img_class: 'repair_img' },
      #{ name: 'Plumbing', img_class: 'repair_img' },
      #{ name: 'Painting', img_class: 'repair_img' },
      #{ name: 'Electrician', img_class: 'repair_img' },
      #{ name: 'Installation', img_class: 'repair_img' },

      #{ name: 'Personal Shopping', img_class: 'shopping_img' },

      #{ name: 'Pedicure', img_class: 'salon_img' },
      #{ name: 'Hair Dye', img_class: 'salon_img' },
      #{ name: 'Hair Cut', img_class: 'salon_img' },
      #{ name: 'Massage', img_class: 'salon_img' },

      #{ name: 'Basic Tutorial', img_class: 'babysitting_img' },

      #{ name: 'Payments', img_class: 'legal_aide_img' },
      #{ name: 'Delivery', img_class: 'legal_aide_img' },
      #{ name: 'Encoding', img_class: 'legal_aide_img' },

      ##{ name: 'Driving', img_class: 'driving_img' },
      #{ name: 'Cooking', img_class: 'cooking_img' }

      #{ name: 'General House Cleaning', img_class: 'cleaning_img' },
      #{ name: 'Refrigerator Repair', img_class: 'repair_img' },
      #{ name: 'Grocery Shopping', img_class: 'shopping_img' },
      #{ name: 'NBI Clearance', img_class: 'legal_aide_img' },
      #{ name: 'Manicure', img_class: 'salon_img' },
      #{ name: 'Babysitting', img_class: 'babysitting_img' },
      #{ name: 'Minor Alterations', img_class: 'alterations_img' },


      #{ name: 'Bathroom Cleaning', img_class: 'cleaning_img' },
      #{ name: 'Car Wash', img_class: 'cleaning_img' },
      #{ name: 'Laundry', img_class: 'cleaning_img' },

      #{ name: 'Aircon Repair', img_class: 'repair_img' },
      #{ name: 'Computer Repair', img_class: 'repair_img' },
      #{ name: 'TV Repair', img_class: 'repair_img' },
      #{ name: 'Plumbing', img_class: 'repair_img' },
      #{ name: 'Painting', img_class: 'repair_img' },
      #{ name: 'Electrician', img_class: 'repair_img' },

      #{ name: 'Personal Shopping', img_class: 'shopping_img' },

      #{ name: 'BIR Form', img_class: 'legal_aide_img' },
      #{ name: 'NSO', img_class: 'legal_aide_img' },
      #{ name: 'Brgy Clearance', img_class: 'legal_aide_img' },

      #{ name: 'Pedicure', img_class: 'salon_img' },
      #{ name: 'Hair Color', img_class: 'salon_img' },
      #{ name: 'Hair Cut', img_class: 'salon_img' },

      #{ name: 'Driving', img_class: 'driving_img' },
      #{ name: 'Driving', img_class: 'driving_img' }

      #{ name: 'Cooking', img_class: 'cooking_img' },
      #{ name: 'Cleaning', img_class: 'cleaning_img' },
      #{ name: 'Laundry', img_class: 'laundry_img' },
      #{ name: 'Escorting', img_class: 'escorting_img' },
      #{ name: 'Washing', img_class: 'washing_img' },
      #{ name: 'Lifting', img_class: 'lifting_img' }
    ]

    $scope.init = ->
      initTypeAhead()
      true

    initTypeAhead = ->
      dataset = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: states
      })

      $('#searchHelp').typeahead({
        hint: true,
        highlight: true,
        minLength: 0
      },
      {
        name: 'states',
        source: (q, sync) ->
          if q == ''
            sync(states.slice(0, 5))
          else
            dataset.search(q, sync)
        templates: {
          suggestion: (data) ->
            href = if data.url? then data.url else '/location'
            '<div class="sb-results-item">
               <a class="js-sb--template-link" data-ignore="true" href="' + href + '" data-no-turbolink>
                 <div class="media--object">
                   <div class="media--figure">
                     <div class="sb-results__results-item__img '+ data.img_class + '"></div>
                   </div>
                  <div class="media--content">' + data.name + '</div>
                 </div>
               </a>
             </div>'
        }
      }).on('typeahead:selected', (obj, datum) ->
        #$('#task_details').modal('toggle')
        #alert('rawr')
        false
      )

    $scope.init()
])
