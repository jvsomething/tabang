@app = angular.module('Tabang', [
  'ngMap',
  'ui.bootstrap.datetimepicker'
])

$(document).on 'turbolinks:load', ->
  angular.bootstrap document.body, ['Tabang']
