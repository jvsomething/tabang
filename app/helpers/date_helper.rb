module DateHelper

  def header_format(date)
    date.strftime('%b %-d, %Y - %a')
  end
end
