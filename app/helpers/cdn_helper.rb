module CdnHelper

  BOOTSTRAP_VERSION = '3.3.7'
  JQUERY_VERSION = '3.2.1'
  ANGULR_VERSION = '1.6.4'

  def use_cdn?
    true
  end

  def bootstrap_css_settings
    {
      media: 'all',
      integrity: 'sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u',
      crossorigin: 'anonymous'
    }
  end

  def bootstrap_js_settings
    {
      integrity: 'sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa',
      crossorigin: 'anonymous'
    }
  end

  def bootstrap_cdn_url(type)
    'https://maxcdn.bootstrapcdn.com/bootstrap/' + BOOTSTRAP_VERSION + '/' + type + '/bootstrap.min.' + type
  end

  def jquery_cdn_url
    'https://ajax.googleapis.com/ajax/libs/jquery/' + JQUERY_VERSION + '/jquery.min.js'
  end

  def angular_cdn_url
    'https://ajax.googleapis.com/ajax/libs/angularjs/' + ANGULR_VERSION + '/angular.min.js'
  end
end
