module ApplicationHelper
  INGREDIENTS = [
    'Apple',
    'Apricot',
    'Asparagus',
    'Aubergine',
    'Avocado',
    'Banana',
    'Beetroot',
    'Black-eye bean',
    'Broad bean',
    'Broccoli',
    'Brussels sprout',
    'Butternut Squash',
    'Carrot',
    'Cherry',
    'Clementine',
    'Courgette',
    'Date',
    'Elderberry',
    'Endive',
    'Fennel',
    'Fig',
    'Garlic',
    'Grape',
    'Green bean',
    'Guava',
    'Haricot bean',
    'Honeydew melon',
    'Iceberg lettuce',
    'Jerusalem artichoke',
    'Kiwi fruit',
    'Leek',
    'Lemon',
    'Mango',
    'Melon',
    'Mushroom',
    'Nectarine',
    'Nut',
    'Olive',
    'Orange',
    'Pea',
    'Peanut',
    'Pear',
    'Pepper',
    'Pineapple',
    'Pumpkin',
    'Quince',
    'Radish',
    'Raisin',
    'Rhubarb',
    'Satsuma',
    'Strawberry',
    'Sweet potato',
    'Tomato',
    'Turnip',
    'Ugli fruit',
    'Victoria plum',
    'Vine leaf',
    'Watercress',
    'Watermelon',
    'Yam',
    'Zucchini'
  ]



  MEALS = [
    { name: 'Meatloaf', img_url: 'http://food.fnr.sndimg.com/content/dam/images/food/fullset/2010/4/13/0/GC_good-eats-meatloaf_s4x3.jpg.rend.hgtvcom.616.462.suffix/1380061114628.jpeg', price: 100 },
    { name: 'Honey Baked Chicken', img_url: 'http://foodell.com/images/recipes/Honey_soy_chicken.jpg', price: 150  },
    { name: 'Salisbury Steak', img_url: 'http://cincyshopper.com/wp-content/uploads/2015/10/Simple-Salisbury-Steak-11.jpg', price: 200 },
    { name: 'Taco Salad', img_url: 'http://food.fnr.sndimg.com/content/dam/images/food/fullset/2012/9/4/0/FNM_100112-Almost-Famous-Steak-Taco-Salad-Recipe_s4x3.jpg.rend.hgtvcom.616.462.suffix/1371609332863.jpeg', price: 93 },
    { name: 'Chicken Fajitas', img_url: 'https://www.ndtv.com/cooks/images/chicken-fajitas.jpg', price: 100 },
    { name: 'Corn Chowder', img_url: 'http://food.fnr.sndimg.com/content/dam/images/food/fullset/2003/9/29/0/ig1a13_corn_chowder.jpg.rend.hgtvcom.616.462.suffix/1485462146643.jpeg', price: 86 },
    { name: 'Cheeseburger Soup', img_url: 'https://cdn2.tmbi.com/TOH/Images/Photos/37/300x300/Cheeseburger-Soup_EXPS_SBZ17_2832_B10_21_1b.jpg', price: 132 },
    { name: 'Chicken Caesar Salad', img_url: 'http://food.fnr.sndimg.com/content/dam/images/food/fullset/2010/3/3/0/FNM_040110-W-N-Dinners-015_s4x3.jpg.rend.hgtvcom.616.462.suffix/1371591380107.jpeg', price: 112 },
    { name: 'Spaghetti', img_url: 'http://www.simplyrecipes.com/wp-content/uploads/2006/09/italian-sausage-spaghetti-horiz-640.jpg', price: 123 },
    { name: 'Pizza', img_url: 'https://cdn.modpizza.com/wp-content/uploads/2016/11/mod-pizza-mad-dog-e1479167997381.png', price: 122 },
    { name: 'Calzones', img_url: 'http://s3.amazonaws.com/finecooking.s3.tauntonclud.com/app/uploads/2017/04/18190744/051092070-01-calzone-recipe-main.jpg', price: 99 },
    { name: 'Stuffed Shells & Peppers', img_url: 'http://4.bp.blogspot.com/-gopxeQfK158/UyJO8Xbk6MI/AAAAAAAAFWk/q4aPuxvPnrs/s1600/Sausage-Stuffed-Shells-3.jpg', price: 135 },
    { name: 'Chicken Divan', img_url: 'https://images-gmi-pmc.edge-generalmills.com/63c052f9-6b0c-4291-9d4f-f0aa4cff4b0c.jpg', price: 111 },
    { name: 'Quiche', img_url: 'http://food.fnr.sndimg.com/content/dam/images/food/fullset/2015/8/4/1/VB0102H_Quiche-Valerie_s4x3.jpg.rend.hgtvcom.616.462.suffix/1438736464664.jpeg', price: 153 },
    { name: 'White Garlic Lasagna', img_url: 'http://tastykitchen.com/wp-content/uploads/2013/01/Tasty-Kitchen-Blog-White-Cheese-and-Chicken-Lasagna.jpg', price: 90 },
    { name: 'Macaroni & Cheese', img_url: 'https://lh6.googleusercontent.com/-zgsC9661HqE/T84vD55kg3I/AAAAAAAAHEc/ozqe4On72sw/s640/Miracle+Mac+front.jpg', price: 93 },
    { name: 'Grilled Chicken', img_url: 'http://food.fnr.sndimg.com/content/dam/images/food/fullset/2009/4/14/2/FNM060109WE0421_s4x3.jpg.rend.hgtvcom.616.462.suffix/1371589415669.jpeg', price: 78 },
    { name: 'Burgers', img_url: 'https://images.fastcompany.net/image/upload/w_596,c_limit,q_auto:best,f_auto,fl_lossy/wp-cms/uploads/2017/06/i-1-sonic-burger.jpg', price: 135 },
    { name: 'Enchiladas', img_url: 'http://food.fnr.sndimg.com/content/dam/images/food/fullset/2012/10/1/0/WU0308H_simple-perfect-enchiladas_s4x3.jpg.rend.hgtvcom.616.462.suffix/1382541970364.jpeg', price: 123 },
    { name: 'Mexican Lasagna', img_url: 'https://images-gmi-pmc.edge-generalmills.com/4d613857-4401-44c7-aae6-c50d16359755.jpg', price: 89 }
  ].freeze

  def random_meal(size)
    MEALS.sample(size)
  end

  def ingredient_list
    INGREDIENTS
  end
end
